﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_date_time
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(13, 13)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(131, 42)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Date Now"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(150, 13)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(131, 42)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "Year"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(424, 13)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(131, 42)
        Me.Button3.TabIndex = 0
        Me.Button3.Text = "Day"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(561, 12)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(131, 42)
        Me.Button4.TabIndex = 0
        Me.Button4.Text = "Time"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(287, 13)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(131, 42)
        Me.Button5.TabIndex = 0
        Me.Button5.Text = "Month"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(13, 73)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(131, 42)
        Me.Button7.TabIndex = 0
        Me.Button7.Text = "Year++"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(150, 73)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(131, 42)
        Me.Button8.TabIndex = 0
        Me.Button8.Text = "Month++"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(287, 73)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(131, 42)
        Me.Button9.TabIndex = 0
        Me.Button9.Text = "Day++"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(424, 73)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(131, 42)
        Me.Button10.TabIndex = 0
        Me.Button10.Text = "Time++"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(561, 73)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(131, 42)
        Me.Button6.TabIndex = 0
        Me.Button6.Text = "Days a Year"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(13, 126)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(131, 42)
        Me.Button11.TabIndex = 0
        Me.Button11.Text = "Days a Week"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(287, 126)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(131, 42)
        Me.Button12.TabIndex = 0
        Me.Button12.Text = "Kind"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(424, 126)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(131, 42)
        Me.Button13.TabIndex = 0
        Me.Button13.Text = "Tick"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(150, 126)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(131, 42)
        Me.Button14.TabIndex = 0
        Me.Button14.Text = "Date Time Universal"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'form_date_time
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(695, 180)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "form_date_time"
        Me.Text = "form_date_time"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Button7 As Button
    Friend WithEvents Button8 As Button
    Friend WithEvents Button9 As Button
    Friend WithEvents Button10 As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents Button11 As Button
    Friend WithEvents Button12 As Button
    Friend WithEvents Button13 As Button
    Friend WithEvents Button14 As Button
End Class
