﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Menu_Master
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_loading = New System.Windows.Forms.Button()
        Me.btn_left_mid_right = New System.Windows.Forms.Button()
        Me.btn_message = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btn_loading
        '
        Me.btn_loading.Location = New System.Drawing.Point(211, 12)
        Me.btn_loading.Name = "btn_loading"
        Me.btn_loading.Size = New System.Drawing.Size(193, 44)
        Me.btn_loading.TabIndex = 0
        Me.btn_loading.Text = "Loading"
        Me.btn_loading.UseVisualStyleBackColor = True
        '
        'btn_left_mid_right
        '
        Me.btn_left_mid_right.Location = New System.Drawing.Point(410, 11)
        Me.btn_left_mid_right.Name = "btn_left_mid_right"
        Me.btn_left_mid_right.Size = New System.Drawing.Size(193, 44)
        Me.btn_left_mid_right.TabIndex = 0
        Me.btn_left_mid_right.Text = "Left Mid Right"
        Me.btn_left_mid_right.UseVisualStyleBackColor = True
        '
        'btn_message
        '
        Me.btn_message.Location = New System.Drawing.Point(609, 11)
        Me.btn_message.Name = "btn_message"
        Me.btn_message.Size = New System.Drawing.Size(193, 44)
        Me.btn_message.TabIndex = 0
        Me.btn_message.Text = "Message Box"
        Me.btn_message.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(12, 12)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(193, 44)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Form Position"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(410, 112)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(193, 44)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Calculator"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(12, 62)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(193, 44)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Data Grid"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(609, 62)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(193, 44)
        Me.Button4.TabIndex = 2
        Me.Button4.Text = "Looping"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(211, 62)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(193, 44)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "DateTime"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(12, 112)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(193, 44)
        Me.Button6.TabIndex = 3
        Me.Button6.Text = "Frame"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(211, 112)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(193, 44)
        Me.Button7.TabIndex = 3
        Me.Button7.Text = "Form"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(410, 62)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(193, 44)
        Me.Button8.TabIndex = 3
        Me.Button8.Text = "Timer"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(609, 112)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(193, 44)
        Me.Button9.TabIndex = 4
        Me.Button9.Text = "Picture"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(12, 162)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(193, 44)
        Me.Button10.TabIndex = 5
        Me.Button10.Text = "File"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(211, 162)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(193, 44)
        Me.Button11.TabIndex = 6
        Me.Button11.Text = "Button"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(410, 162)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(193, 44)
        Me.Button12.TabIndex = 7
        Me.Button12.Text = "Chart"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(609, 162)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(193, 44)
        Me.Button13.TabIndex = 8
        Me.Button13.Text = "Panel"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Menu_Master
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(814, 216)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btn_message)
        Me.Controls.Add(Me.btn_left_mid_right)
        Me.Controls.Add(Me.btn_loading)
        Me.Name = "Menu_Master"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menu_Master"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btn_loading As Button
    Friend WithEvents btn_left_mid_right As Button
    Friend WithEvents btn_message As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents Button7 As Button
    Friend WithEvents Button8 As Button
    Friend WithEvents Button9 As Button
    Friend WithEvents Button10 As Button
    Friend WithEvents Button11 As Button
    Friend WithEvents Button12 As Button
    Friend WithEvents Button13 As Button
End Class
