﻿Public Class Form_Calculator
    Dim first_number As Double
    Dim second_number As Double
    Dim btn_operator As String
    Dim total_value As Double
    Dim dot As String
    Private Sub Btn_0_Click(sender As Object, e As EventArgs) Handles btn_0.Click
        If first_number = 0 Then
            text_display.Text = 0
        Else
            text_display.Text = text_display.Text & 0
        End If
        first_number = 0
    End Sub

    Private Sub Form_Operator_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Btn_1_Click(sender As Object, e As EventArgs) Handles btn_1.Click
        text_display.Text = text_display.Text & 1
    End Sub

    Private Sub Btn_2_Click(sender As Object, e As EventArgs) Handles btn_2.Click
        text_display.Text = text_display.Text & 2
    End Sub

    Private Sub Btn_3_Click(sender As Object, e As EventArgs) Handles btn_3.Click
        text_display.Text = text_display.Text & 3
    End Sub

    Private Sub Btn_4_Click(sender As Object, e As EventArgs) Handles btn_4.Click
        text_display.Text = text_display.Text & 4
    End Sub

    Private Sub Btn_5_Click(sender As Object, e As EventArgs) Handles btn_5.Click
        text_display.Text = text_display.Text & 5
    End Sub

    Private Sub Btn_6_Click(sender As Object, e As EventArgs) Handles btn_6.Click
        text_display.Text = text_display.Text & 6
    End Sub

    Private Sub Btn_7_Click(sender As Object, e As EventArgs) Handles btn_7.Click
        text_display.Text = text_display.Text & 7
    End Sub

    Private Sub Btn__8_Click(sender As Object, e As EventArgs) Handles btn__8.Click
        text_display.Text = text_display.Text & 8
    End Sub

    Private Sub Btn_9_Click(sender As Object, e As EventArgs) Handles btn_9.Click
        text_display.Text = text_display.Text & 9
    End Sub

    Private Sub Btn_dot_Click(sender As Object, e As EventArgs) Handles btn_dot.Click

    End Sub

    Private Sub Btn_plus_Click(sender As Object, e As EventArgs) Handles btn_plus.Click
        first_number = text_display.Text
        btn_operator = "plus"
        text_display.Text = ""
    End Sub

    Private Sub Btn_equal_Click(sender As Object, e As EventArgs) Handles btn_equal.Click
        second_number = text_display.Text
        If btn_operator = "plus" Then
            total_value = first_number + second_number
        ElseIf btn_operator = "minus" Then
            total_value = first_number - second_number
        ElseIf btn_operator = "divided" Then
            total_value = first_number / second_number
        ElseIf btn_operator = "multiplies" Then
            total_value = first_number * second_number
        End If
        text_display.Text = total_value
    End Sub

    Private Sub Btn_x_Click(sender As Object, e As EventArgs) Handles btn_x.Click
        btn_operator = "multiplies"
        first_number = text_display.Text
        text_display.Text = ""
    End Sub

    Private Sub Btn_minus_Click(sender As Object, e As EventArgs) Handles btn_minus.Click
        btn_operator = "minus"
        first_number = text_display.Text
        text_display.Text = ""
    End Sub

    Private Sub Btn_divided_Click(sender As Object, e As EventArgs) Handles btn_divided.Click
        btn_operator = "divided"
        first_number = text_display.Text
        text_display.Text = ""
    End Sub

    Private Sub Btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click
        btn_operator = ""
        first_number = 0
        second_number = 0
        text_display.Text = ""
    End Sub

    Private Sub Btn_clearE_Click(sender As Object, e As EventArgs) Handles btn_clearE.Click
        text_display.Text = ""
    End Sub

    Private Sub Btn_negatif_plus_Click(sender As Object, e As EventArgs) Handles btn_negatif_plus.Click
        text_display.Text = text_display.Text * -1
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub
End Class