﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Calculator
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.text_display = New System.Windows.Forms.TextBox()
        Me.btn_1 = New System.Windows.Forms.Button()
        Me.btn_2 = New System.Windows.Forms.Button()
        Me.btn_3 = New System.Windows.Forms.Button()
        Me.btn_4 = New System.Windows.Forms.Button()
        Me.btn_5 = New System.Windows.Forms.Button()
        Me.btn_6 = New System.Windows.Forms.Button()
        Me.btn_7 = New System.Windows.Forms.Button()
        Me.btn__8 = New System.Windows.Forms.Button()
        Me.btn_9 = New System.Windows.Forms.Button()
        Me.btn_divided = New System.Windows.Forms.Button()
        Me.btn_x = New System.Windows.Forms.Button()
        Me.btn_minus = New System.Windows.Forms.Button()
        Me.btn_plus = New System.Windows.Forms.Button()
        Me.btn_equal = New System.Windows.Forms.Button()
        Me.btn_modulus = New System.Windows.Forms.Button()
        Me.btn_negatif_plus = New System.Windows.Forms.Button()
        Me.btn_0 = New System.Windows.Forms.Button()
        Me.btn_dot = New System.Windows.Forms.Button()
        Me.btn_clearE = New System.Windows.Forms.Button()
        Me.btn_clear = New System.Windows.Forms.Button()
        Me.btn_delete = New System.Windows.Forms.Button()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'text_display
        '
        Me.text_display.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_display.Location = New System.Drawing.Point(25, 29)
        Me.text_display.Multiline = True
        Me.text_display.Name = "text_display"
        Me.text_display.Size = New System.Drawing.Size(686, 94)
        Me.text_display.TabIndex = 0
        '
        'btn_1
        '
        Me.btn_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_1.Location = New System.Drawing.Point(25, 373)
        Me.btn_1.Name = "btn_1"
        Me.btn_1.Size = New System.Drawing.Size(172, 55)
        Me.btn_1.TabIndex = 1
        Me.btn_1.Text = "1"
        Me.btn_1.UseVisualStyleBackColor = True
        '
        'btn_2
        '
        Me.btn_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_2.Location = New System.Drawing.Point(203, 373)
        Me.btn_2.Name = "btn_2"
        Me.btn_2.Size = New System.Drawing.Size(172, 55)
        Me.btn_2.TabIndex = 1
        Me.btn_2.Text = "2"
        Me.btn_2.UseVisualStyleBackColor = True
        '
        'btn_3
        '
        Me.btn_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_3.Location = New System.Drawing.Point(381, 373)
        Me.btn_3.Name = "btn_3"
        Me.btn_3.Size = New System.Drawing.Size(172, 55)
        Me.btn_3.TabIndex = 1
        Me.btn_3.Text = "3"
        Me.btn_3.UseVisualStyleBackColor = True
        '
        'btn_4
        '
        Me.btn_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_4.Location = New System.Drawing.Point(25, 312)
        Me.btn_4.Name = "btn_4"
        Me.btn_4.Size = New System.Drawing.Size(172, 55)
        Me.btn_4.TabIndex = 1
        Me.btn_4.Text = "4"
        Me.btn_4.UseVisualStyleBackColor = True
        '
        'btn_5
        '
        Me.btn_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_5.Location = New System.Drawing.Point(203, 312)
        Me.btn_5.Name = "btn_5"
        Me.btn_5.Size = New System.Drawing.Size(172, 55)
        Me.btn_5.TabIndex = 1
        Me.btn_5.Text = "5"
        Me.btn_5.UseVisualStyleBackColor = True
        '
        'btn_6
        '
        Me.btn_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_6.Location = New System.Drawing.Point(381, 312)
        Me.btn_6.Name = "btn_6"
        Me.btn_6.Size = New System.Drawing.Size(172, 55)
        Me.btn_6.TabIndex = 1
        Me.btn_6.Text = "6"
        Me.btn_6.UseVisualStyleBackColor = True
        '
        'btn_7
        '
        Me.btn_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_7.Location = New System.Drawing.Point(25, 251)
        Me.btn_7.Name = "btn_7"
        Me.btn_7.Size = New System.Drawing.Size(172, 55)
        Me.btn_7.TabIndex = 1
        Me.btn_7.Text = "7"
        Me.btn_7.UseVisualStyleBackColor = True
        '
        'btn__8
        '
        Me.btn__8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn__8.Location = New System.Drawing.Point(203, 251)
        Me.btn__8.Name = "btn__8"
        Me.btn__8.Size = New System.Drawing.Size(172, 55)
        Me.btn__8.TabIndex = 1
        Me.btn__8.Text = "8"
        Me.btn__8.UseVisualStyleBackColor = True
        '
        'btn_9
        '
        Me.btn_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_9.Location = New System.Drawing.Point(381, 251)
        Me.btn_9.Name = "btn_9"
        Me.btn_9.Size = New System.Drawing.Size(172, 55)
        Me.btn_9.TabIndex = 1
        Me.btn_9.Text = "9"
        Me.btn_9.UseVisualStyleBackColor = True
        '
        'btn_divided
        '
        Me.btn_divided.Location = New System.Drawing.Point(559, 190)
        Me.btn_divided.Name = "btn_divided"
        Me.btn_divided.Size = New System.Drawing.Size(155, 55)
        Me.btn_divided.TabIndex = 2
        Me.btn_divided.Text = "/"
        Me.btn_divided.UseVisualStyleBackColor = True
        '
        'btn_x
        '
        Me.btn_x.Location = New System.Drawing.Point(559, 251)
        Me.btn_x.Name = "btn_x"
        Me.btn_x.Size = New System.Drawing.Size(155, 55)
        Me.btn_x.TabIndex = 2
        Me.btn_x.Text = "X"
        Me.btn_x.UseVisualStyleBackColor = True
        '
        'btn_minus
        '
        Me.btn_minus.Location = New System.Drawing.Point(559, 312)
        Me.btn_minus.Name = "btn_minus"
        Me.btn_minus.Size = New System.Drawing.Size(155, 55)
        Me.btn_minus.TabIndex = 2
        Me.btn_minus.Text = "-"
        Me.btn_minus.UseVisualStyleBackColor = True
        '
        'btn_plus
        '
        Me.btn_plus.Location = New System.Drawing.Point(559, 373)
        Me.btn_plus.Name = "btn_plus"
        Me.btn_plus.Size = New System.Drawing.Size(155, 55)
        Me.btn_plus.TabIndex = 2
        Me.btn_plus.Text = "+"
        Me.btn_plus.UseVisualStyleBackColor = True
        '
        'btn_equal
        '
        Me.btn_equal.Location = New System.Drawing.Point(559, 434)
        Me.btn_equal.Name = "btn_equal"
        Me.btn_equal.Size = New System.Drawing.Size(155, 55)
        Me.btn_equal.TabIndex = 2
        Me.btn_equal.Text = "="
        Me.btn_equal.UseVisualStyleBackColor = True
        '
        'btn_modulus
        '
        Me.btn_modulus.Location = New System.Drawing.Point(556, 129)
        Me.btn_modulus.Name = "btn_modulus"
        Me.btn_modulus.Size = New System.Drawing.Size(155, 55)
        Me.btn_modulus.TabIndex = 2
        Me.btn_modulus.Text = "Mod"
        Me.btn_modulus.UseVisualStyleBackColor = True
        '
        'btn_negatif_plus
        '
        Me.btn_negatif_plus.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_negatif_plus.Location = New System.Drawing.Point(25, 437)
        Me.btn_negatif_plus.Name = "btn_negatif_plus"
        Me.btn_negatif_plus.Size = New System.Drawing.Size(172, 55)
        Me.btn_negatif_plus.TabIndex = 1
        Me.btn_negatif_plus.Text = "+/-"
        Me.btn_negatif_plus.UseVisualStyleBackColor = True
        '
        'btn_0
        '
        Me.btn_0.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_0.Location = New System.Drawing.Point(203, 437)
        Me.btn_0.Name = "btn_0"
        Me.btn_0.Size = New System.Drawing.Size(172, 55)
        Me.btn_0.TabIndex = 1
        Me.btn_0.Text = "0"
        Me.btn_0.UseVisualStyleBackColor = True
        '
        'btn_dot
        '
        Me.btn_dot.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_dot.Location = New System.Drawing.Point(381, 437)
        Me.btn_dot.Name = "btn_dot"
        Me.btn_dot.Size = New System.Drawing.Size(172, 55)
        Me.btn_dot.TabIndex = 1
        Me.btn_dot.Text = "."
        Me.btn_dot.UseVisualStyleBackColor = True
        '
        'btn_clearE
        '
        Me.btn_clearE.Font = New System.Drawing.Font("Century", 8.25!)
        Me.btn_clearE.Location = New System.Drawing.Point(25, 190)
        Me.btn_clearE.Name = "btn_clearE"
        Me.btn_clearE.Size = New System.Drawing.Size(172, 55)
        Me.btn_clearE.TabIndex = 2
        Me.btn_clearE.Text = "CE"
        Me.btn_clearE.UseVisualStyleBackColor = True
        '
        'btn_clear
        '
        Me.btn_clear.Font = New System.Drawing.Font("Century", 8.25!)
        Me.btn_clear.Location = New System.Drawing.Point(203, 189)
        Me.btn_clear.Name = "btn_clear"
        Me.btn_clear.Size = New System.Drawing.Size(172, 55)
        Me.btn_clear.TabIndex = 2
        Me.btn_clear.Text = "C"
        Me.btn_clear.UseVisualStyleBackColor = True
        '
        'btn_delete
        '
        Me.btn_delete.Font = New System.Drawing.Font("Century", 8.25!)
        Me.btn_delete.Location = New System.Drawing.Point(381, 189)
        Me.btn_delete.Name = "btn_delete"
        Me.btn_delete.Size = New System.Drawing.Size(172, 55)
        Me.btn_delete.TabIndex = 2
        Me.btn_delete.Text = "<X"
        Me.btn_delete.UseVisualStyleBackColor = True
        '
        'Button22
        '
        Me.Button22.Font = New System.Drawing.Font("Century", 8.25!)
        Me.Button22.Location = New System.Drawing.Point(381, 128)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(172, 55)
        Me.Button22.TabIndex = 2
        Me.Button22.Text = "1/x"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.Font = New System.Drawing.Font("Century", 8.25!)
        Me.Button23.Location = New System.Drawing.Point(203, 128)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(172, 55)
        Me.Button23.TabIndex = 2
        Me.Button23.Text = "X2"
        Me.Button23.UseVisualStyleBackColor = True
        '
        'Button24
        '
        Me.Button24.Font = New System.Drawing.Font("Century", 8.25!)
        Me.Button24.Location = New System.Drawing.Point(25, 128)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(172, 55)
        Me.Button24.TabIndex = 2
        Me.Button24.Text = "Akar"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(30, 85)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 29)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "0"
        '
        'Form_Operator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(723, 504)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button24)
        Me.Controls.Add(Me.Button23)
        Me.Controls.Add(Me.Button22)
        Me.Controls.Add(Me.btn_delete)
        Me.Controls.Add(Me.btn_clear)
        Me.Controls.Add(Me.btn_clearE)
        Me.Controls.Add(Me.btn_modulus)
        Me.Controls.Add(Me.btn_equal)
        Me.Controls.Add(Me.btn_plus)
        Me.Controls.Add(Me.btn_minus)
        Me.Controls.Add(Me.btn_x)
        Me.Controls.Add(Me.btn_divided)
        Me.Controls.Add(Me.btn_9)
        Me.Controls.Add(Me.btn__8)
        Me.Controls.Add(Me.btn_6)
        Me.Controls.Add(Me.btn_5)
        Me.Controls.Add(Me.btn_7)
        Me.Controls.Add(Me.btn_3)
        Me.Controls.Add(Me.btn_4)
        Me.Controls.Add(Me.btn_2)
        Me.Controls.Add(Me.btn_dot)
        Me.Controls.Add(Me.btn_0)
        Me.Controls.Add(Me.btn_negatif_plus)
        Me.Controls.Add(Me.btn_1)
        Me.Controls.Add(Me.text_display)
        Me.Name = "Form_Operator"
        Me.Text = "Operator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents text_display As TextBox
    Friend WithEvents btn_1 As Button
    Friend WithEvents btn_2 As Button
    Friend WithEvents btn_3 As Button
    Friend WithEvents btn_4 As Button
    Friend WithEvents btn_5 As Button
    Friend WithEvents btn_6 As Button
    Friend WithEvents btn_7 As Button
    Friend WithEvents btn__8 As Button
    Friend WithEvents btn_9 As Button
    Friend WithEvents btn_divided As Button
    Friend WithEvents btn_x As Button
    Friend WithEvents btn_minus As Button
    Friend WithEvents btn_plus As Button
    Friend WithEvents btn_equal As Button
    Friend WithEvents btn_modulus As Button
    Friend WithEvents btn_negatif_plus As Button
    Friend WithEvents btn_0 As Button
    Friend WithEvents btn_dot As Button
    Friend WithEvents btn_clearE As Button
    Friend WithEvents btn_clear As Button
    Friend WithEvents btn_delete As Button
    Friend WithEvents Button22 As Button
    Friend WithEvents Button23 As Button
    Friend WithEvents Button24 As Button
    Friend WithEvents Label1 As Label
End Class
