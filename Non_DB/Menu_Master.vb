﻿Public Class Menu_Master
    Private Sub Btn_loading_Click(sender As Object, e As EventArgs) Handles btn_loading.Click
        Loading.Show()
    End Sub

    Private Sub Btn_left_mid_right_Click(sender As Object, e As EventArgs) Handles btn_left_mid_right.Click
        Left_Mid_Right.Show()
    End Sub

    Private Sub Btn_message_Click(sender As Object, e As EventArgs) Handles btn_message.Click
        Message_Box.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Form_center.Show()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Form_Calculator.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Form_datagrid.Show()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        form_date_time.Show()
    End Sub
End Class