﻿Public Class Left_Mid_Right
    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        '-------------MENGAMBIL 4 KARAKTER DARI KIRI----------------
        text_left.Text = Microsoft.VisualBasic.Left(TextBox1.Text, 4)

        '-------------MENGAMBIL 4 KARAKTER DARI HURUF KE 2----------------
        text_mid.Text = Microsoft.VisualBasic.Mid(TextBox1.Text, 2, 4)

        '-------------MENGAMBIL 4 KARAKTER DARI KANAN----------------
        text_right.Text = Microsoft.VisualBasic.Right(TextBox1.Text, 4)
    End Sub
End Class