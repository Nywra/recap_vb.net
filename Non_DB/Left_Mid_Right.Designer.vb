﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Left_Mid_Right
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.text_left = New System.Windows.Forms.TextBox()
        Me.text_mid = New System.Windows.Forms.TextBox()
        Me.text_right = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(272, 55)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(238, 26)
        Me.TextBox1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(345, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 20)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Coba Ketikan"
        '
        'text_left
        '
        Me.text_left.Location = New System.Drawing.Point(80, 123)
        Me.text_left.Name = "text_left"
        Me.text_left.Size = New System.Drawing.Size(169, 26)
        Me.text_left.TabIndex = 0
        '
        'text_mid
        '
        Me.text_mid.Location = New System.Drawing.Point(304, 123)
        Me.text_mid.Name = "text_mid"
        Me.text_mid.Size = New System.Drawing.Size(169, 26)
        Me.text_mid.TabIndex = 0
        '
        'text_right
        '
        Me.text_right.Location = New System.Drawing.Point(496, 123)
        Me.text_right.Name = "text_right"
        Me.text_right.Size = New System.Drawing.Size(169, 26)
        Me.text_right.TabIndex = 0
        '
        'Left_Mid_Right
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 161)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.text_right)
        Me.Controls.Add(Me.text_mid)
        Me.Controls.Add(Me.text_left)
        Me.Controls.Add(Me.TextBox1)
        Me.Name = "Left_Mid_Right"
        Me.Text = "Left_Mid_Right"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents text_left As TextBox
    Friend WithEvents text_mid As TextBox
    Friend WithEvents text_right As TextBox
End Class
