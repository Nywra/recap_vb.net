﻿Public Class Message_Box
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        MessageBox.Show("Taraa....Default Message Dengan MessageBox.Show", "Ini Judul")
        MsgBox("Tara.....Default Message Dengan MsgBox", , "Ini Judul") 'Lebih Irit Hehe setelah koma awal type MsgBox nya
    End Sub

    Private Sub Btn_ok_Click(sender As Object, e As EventArgs) Handles btn_ok.Click
        MsgBox("Tara...Ok Only Message", MsgBoxStyle.OkOnly, "Ok Only")
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim jawabannya As String
        jawabannya = MsgBox("Tara...Abort Retry Ignore Message", MsgBoxStyle.AbortRetryIgnore, "Abort Retry Ignore")
        'Hasilnya ada 3 yaitu 3 artinya Abort, 4 artinya Retry, dan 5 artinya Ignore
        Select Case jawabannya
            Case 3
                MsgBox("Kamu Klik Abort")
            Case 4
                MsgBox("Kamu Klik Retry")
            Case 5
                MsgBox("Kamu Klik Ignore")
        End Select
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        MsgBox("Tara...Application Modal Message", MsgBoxStyle.ApplicationModal, "ApplicationModal") 'ntah aku bingung sama ini kaya sama aja
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        MsgBox("Tara...Critical Message", MsgBoxStyle.Critical, "Critical")
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        MsgBox("Tara...Exclamation Message", MsgBoxStyle.Exclamation, "Exclamation")
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        MsgBox("Tara...Information Message", MsgBoxStyle.Information, "Information")
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        MsgBox("Tara...Help Message", MsgBoxStyle.MsgBoxHelp, "Help")
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        MsgBox(TextBox1.Text)
    End Sub
End Class