﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_data_grid_remove
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Data1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.data2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btn_remove = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Data1, Me.data2, Me.btn_remove})
        Me.DataGridView1.Location = New System.Drawing.Point(12, 56)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 28
        Me.DataGridView1.Size = New System.Drawing.Size(776, 354)
        Me.DataGridView1.TabIndex = 0
        '
        'Data1
        '
        Me.Data1.HeaderText = "Data 1"
        Me.Data1.Name = "Data1"
        '
        'data2
        '
        Me.data2.HeaderText = "Data 2"
        Me.data2.Name = "data2"
        '
        'btn_remove
        '
        Me.btn_remove.DataPropertyName = "x"
        Me.btn_remove.HeaderText = "Delete"
        Me.btn_remove.Name = "btn_remove"
        Me.btn_remove.Text = "X"
        Me.btn_remove.ToolTipText = "x"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(13, 13)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(170, 37)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Remove All"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(190, 13)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(224, 37)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Delete Selected Row"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'form_data_grid_remove
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 422)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "form_data_grid_remove"
        Me.Text = "form_data_grid_remove"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Data1 As DataGridViewTextBoxColumn
    Friend WithEvents data2 As DataGridViewTextBoxColumn
    Friend WithEvents btn_remove As DataGridViewButtonColumn
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
End Class
