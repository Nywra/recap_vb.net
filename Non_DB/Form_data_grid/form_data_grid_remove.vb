﻿Public Class form_data_grid_remove
    Private Sub Form_data_grid_remove_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For i As Integer = 0 To 10
            DataGridView1.Rows.Add(New String() {"Data 1 Value " & i, "Data 2 Value " & i})
        Next
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        Dim colName As String = DataGridView1.Columns(e.ColumnIndex).Name
        If colName = "btn_remove" Then
            DataGridView1.Rows.RemoveAt(e.RowIndex)
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        DataGridView1.Rows.Clear()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim i As Integer
        i = DataGridView1.CurrentCell.RowIndex
        DataGridView1.Rows.RemoveAt(i)
    End Sub
End Class