﻿Public Class form_data_grid_numbering
    Private Sub Form_data_grid_numbering_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For i As Integer = 1 To 10
            DataGridView1.Rows.Add(New String() {i, "Data 1 Value " & i, "Data 2 Value " & i})
        Next

        For i As Integer = 1 To 10
            DataGridView2.Rows.Add(New String() {"", "Data 1 Value " & i, "Data 2 Value " & i})
        Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim total_row As Integer = DataGridView2.Rows.Count - 1
        For i As Integer = 0 To total_row
            DataGridView2.Rows(i).Cells(0).Value = i + 1
        Next
    End Sub
End Class