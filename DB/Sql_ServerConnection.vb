﻿Imports System.Data
Imports System.Data.SqlClient

Namespace AksesData
    Public Class DbConnection
        Dim Con As New SqlConnection("Data Source=DESKTOP-COSTT8H\SQLEXPRESS;Initial Catalog=db_users;Integrated Security=True")

        Public Function Open() As SqlConnection
            Try
                Con.Open()
                Return Con
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End Function

        Public Sub Close()
            Con.Close()
        End Sub
    End Class
End Namespace
