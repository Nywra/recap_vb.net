﻿Public Class after_login_role
    Public Property role As String 'Lemparan Parameter'
    Private Sub After_login_role_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If role = 1 Then
            lbl_role.Text = "Master"
            Button1.Enabled = True
        ElseIf role = 2 Then
            lbl_role.Text = "Pedagang"
            Button2.Enabled = True
        Else
            lbl_role.Text = "Anak Hilang"
            Button3.Enabled = True
        End If
    End Sub
End Class